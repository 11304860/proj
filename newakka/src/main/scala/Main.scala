import akka.actor.typed.scaladsl.AskPattern.{Askable, schedulerFromActorSystem}
import akka.actor.typed.{ActorRef, ActorSystem}
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.marshalling._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.unmarshalling._
import akka.util.Timeout
import io.swagger.server.api.serve.ActeurForm
import io.swagger.server.api.serve.ActeurForm.GetForms
import io.swagger.server.api.{DefaultApi, DefaultApiMarshaller, DefaultApiService, serve, server}
import io.swagger.server.model
import io.swagger.server.model.{Form, Tache}
import spray.json.DefaultJsonProtocol.{BooleanJsonFormat, StringJsonFormat, jsonFormat11, jsonFormat6}
import spray.json.RootJsonFormat
import spray.json._
import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContext, Future}
import scala.io.StdIn

object Main extends App {

  implicit val system: ActorSystem[server.MonActeur.Message] = ActorSystem(server.MonActeur.apply, "MonActeur")

  // needed for the future flatMap/onComplete in the end
  implicit val executionContext: ExecutionContext = {system}.executionContext

  val monActeur1: ActorRef[serve.ActeurForm.Message] = system.systemActorOf(ActeurForm.apply,"monActeur1")
  val monActeur: ActorRef[server.MonActeur.Message] = system

  import server.MonActeur._


  object DefaultMarshaller extends DefaultApiMarshaller with SprayJsonSupport {


    // implicit def creerClientsFormat: RootJsonFormat[Form] = jsonFormat5(Form)
    // implicit def ChoixFormat: RootJsonFormat[Choix] = jsonFormat3(Choix)


    implicit def tacheFormat: RootJsonFormat[Tache] = jsonFormat6(Tache)
    implicit def FormFormat: RootJsonFormat[Form] = jsonFormat11(Form)


    implicit val fromEntityUnmarshallerForm: FromEntityUnmarshaller[Form] = jsonFormat11(Form)
    implicit val toEntityMarshallerForm: ToEntityMarshaller[Form] = jsonFormat11(Form)
    implicit val toEntityMarshallerTache: ToEntityMarshaller[Tache] = jsonFormat6(Tache)
    implicit val fromEntityUnmarshallerTache: FromEntityUnmarshaller[Tache] = jsonFormat6(Tache)
    implicit val fromEntityUnmarshallerListForm: FromEntityUnmarshaller[List[Form]] = listFormat(jsonFormat11(Form))



  }


  object DefaultService extends DefaultApiService {
    /*  def creerEtudiants(form: Form)
   (implicit  toEntityMarshallerForm: ToEntityMarshaller[Form]) :Route ={

}*/

    def getSuggestion(id: Int)(implicit toEntityMarshallerTache: ToEntityMarshaller[Tache]): Route = {

      implicit val timeout: Timeout = 5.seconds
      // query the actor for the current MonActeur state
      val forms: Future[Tache] = monActeur.ask(ref => GetTache(id, ref))

      requestcontext => {
        (forms).flatMap {
          (tache: Tache) =>
            getSuggestion200(tache)(toEntityMarshallerTache)(requestcontext)
        }


      }
    }

    //   def getUser(id: Int)(implicit toEntityMarshallerForm: ToEntityMarshaller[model.Form]): Route = {
    // implicit val timeout: Timeout = 5.seconds
    //     // query the actor for the current MonActeur state

    //       val form: Future[Form] = monActeur.ask(ref => GetForm(id, ref))

    //      requestcontext => {
    //         (form).flatMap {
    //           (form: Form) =>
    //           getUser200(form)(toEntityMarshallerForm)(requestcontext)
    //         }
    //   }
    // }
    // Méthode qui permet de récuperer la requette post
    def creerSuggestion(tache: model.Tache): Route = {
      monActeur ! tache
      requestcontext => creerSuggestion200(requestcontext)
    }

    override def formulairePost(body: Form): Route = {
      monActeur1 ! body
      requestcontext => formulairePost200(requestcontext)
    }

    /**
     * Code: 200, Message: Retoune les formulaires des etudiants, DataType: Form
     * Code: 404, Message: Not found
     */
    override def formulairesGet()(implicit toEntityMarshallerForm: ToEntityMarshaller[Form]): Route = {

      implicit val timeout: Timeout = 5.seconds
      // query the actor for the current MonActeur state
      val forms: Future[List[Form]] = monActeur1.ask(ref => GetForms(ref))

      requestcontext => {
        (forms).flatMap {
          (form: List[Form]) =>
            formulairesGet200(form)(toEntityMarshallerForm)(requestcontext)
        }


      }
    }

    override def getFormulaire(numEtudiant: Int)(implicit toEntityMarshallerTache: ToEntityMarshaller[Form]): Route = ???
  }

  /*
def creerFormulaire(tache: model.Form): Route = {
   monActeur ! tache
   requestcontext => creerSuggestion200(requestcontext)
  }

}

*/

  val api = new DefaultApi(DefaultService, DefaultMarshaller)


  val host = "localhost"
  val port = 4081

  val bindingFuture = Http().newServerAt(host, port).bind(api.route)
  println(s"Server online at http://${host}:${port}/\nPress RETURN to stop...")

  bindingFuture.failed.foreach { ex =>
    println(s"${ex} Failed to bind to ${host}:${port}!")
  }

  StdIn.readLine() // let it run until user presses return
  bindingFuture
    .flatMap(_.unbind()) // trigger unbinding from the port
    .onComplete(_ => system.terminate())


}
