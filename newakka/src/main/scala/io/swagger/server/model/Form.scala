package io.swagger.server.model

import io.swagger.server.api.serve.ActeurForm

/**
 * @param numEtudiant 
 * @param nom 
 * @param prenom 
 * @param age 
 * @param sexe 
 * @param formation 
 * @param entreprise 
 * @param fonction 
 * @param remuneration 
 * @param ville 
 * @param avantages 
 */
final case class Form (
  numEtudiant: String,
  nom: String,
  prenom: String,
  age: String,
  sexe: String,
  formation: String,
  entreprise: String,
  fonction: String,
  remuneration: String,
  ville: String,
  avantages: String
) extends ActeurForm.Message

