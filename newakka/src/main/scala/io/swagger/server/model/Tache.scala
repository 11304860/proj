package io.swagger.server.model

import io.swagger.server.api.server.MonActeur

/**
 * @param id 
 * @param module 
 * @param date_debut 
 * @param date_fin 
 * @param description 
 * @param realisee 
 */
final case class Tache (
  id: String,
  module: String,
  date_debut: String,
  date_fin: String,
  description: String,
  realisee: Boolean
) extends MonActeur.Message

