package io.swagger.server.api


import akka.actor.typed.scaladsl.AskPattern._
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ ActorRef, ActorSystem }
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.util.Timeout
import spray.json.DefaultJsonProtocol._

import scala.concurrent.duration._
import scala.concurrent.{ ExecutionContext, Future }
import scala.io.StdIn

import io.swagger.server.model._
import akka.pattern.StatusReply
//import java.text.Normalizer.Form

object serve {
object ActeurForm{
    trait Message


    case class GetForms(replyTo: ActorRef[List[Form]]) extends Message
    case class GetFormDelete(id: Int,replyTo: ActorRef[List[Form]]) extends Message


    def apply: Behaviors.Receive[Message] = apply(List.empty)

    def apply(formulaires: List[Form]): Behaviors.Receive[Message] = Behaviors.receive {
        case(ctx, formulaire @ Form(numEtudiant, nom,  prenom, age, sexe, formation, entreprise, fonction, remuneration, ville, avantages))=>
            ctx.log.info(s"Form complete: $numEtudiant, $nom,  $prenom, $age, $sexe, $formation, $entreprise, $fonction, $remuneration, $ville, $avantages")
            apply(formulaires :+ formulaire)
        case(_, GetForms(replyTo))=>
            replyTo ! formulaires
            Behaviors.same
        case(_, GetFormDelete(id, replyTo) )=>
            val index = formulaires.indexWhere(f => f.numEtudiant.equals(id))
            replyTo ! formulaires.drop(index)
            Behaviors.same
    }
}
}