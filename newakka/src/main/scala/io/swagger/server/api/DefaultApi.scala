package io.swagger.server.api

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.unmarshalling.FromRequestUnmarshaller
import akka.http.scaladsl.marshalling.ToEntityMarshaller
import io.swagger.server.AkkaHttpHelper._
import io.swagger.server.model.Tache
import io.swagger.server.model.Form

import akka.http.scaladsl.unmarshalling.FromEntityUnmarshaller

class DefaultApi(
    defaultService: DefaultApiService,
    defaultMarshaller: DefaultApiMarshaller
) {
  import defaultMarshaller._

  lazy val route: Route =
  path("suggestion") { 
      post {  
            entity(as[Tache]){ tache =>
              defaultService.creerSuggestion(tache)
            }
      }
    }

  path("suggestion" / IntNumber) { (id) => 
      get {
              
             defaultService.getSuggestion(id = id)
                     
            }
         
  }

  path("formulaires") { 
      get {
        defaultService.formulairesGet()
      }

  }
  path("formulaire") { 
      post {
                entity(as[Form]){ body =>
                  defaultService.formulairePost(body)
                }
             
            }
         
        
      }
  

  path("formulaire/delete"/ IntNumber) { (id) => 
      delete {
                     
                  defaultService.getFormulaire(id) 
            }
    }

}

trait DefaultApiService {

    def creerSuggestion200: Route =
    complete((200, "Tache créés."))
  def creerSuggestion400: Route =
    complete((400, "La tache n&#39;a pas été créés."))

    
  /**
   * Code: 200, Message: Etudiant créés.
   * Code: 400, Message: Letudiant n&#39;a pas été créés.
   */

   def formulairePost200: Route =
    complete((200, "formulaire crée."))
  def formulairePost400: Route =
    complete((400, "le formulaire n&#x27;a pas ete créer."))
  /**
   * Code: 200, Message: formulaire crée.
   * Code: 400, Message: le formulaire n&#x27;a pas ete créer.
   */

  def creerSuggestion(tache: Tache): Route

  def formulairePost(body: Form): Route

  def getSuggestion200(responseTache: Tache)(implicit toEntityMarshallerTache: ToEntityMarshaller[Tache]): Route =
    complete((200, responseTache))
  def getSuggestion404: Route =
    complete((404, "Not found"))
  /**
   * Code: 200, Message: Retoune la tâche demandée, DataType: Tache
   * Code: 404, Message: Not found
   */

   def formulairesGet200(responseForm: List[Form])(implicit toEntityMarshallerForm: ToEntityMarshaller[List[Form]]): Route =
    complete((200, responseForm))
  def formulairesGet404: Route =
    complete((404, "Not found"))
  /**
   * Code: 200, Message: Retoune les formulaires des etudiants, DataType: Form
   * Code: 404, Message: Not found
   */

   def formulairesGet()
      (implicit toEntityMarshallerForm: ToEntityMarshaller[List[Form]]): Route

  def getFormulaire200: Route =
    complete((200, "formulaire supprimer"))
  def getFormulaire400: Route =
    complete((400, "le formulaire n&#x27;a pas ete créer."))
  /**
   * Code: 200, Message: formulaire supprimer
   * Code: 400, Message: le formulaire n&#x27;a pas ete créer.
   */


  def getSuggestion(id: Int)
      (implicit toEntityMarshallerTache: ToEntityMarshaller[Tache]): Route


  def getFormulaire(numEtudiant: Int)
      (implicit toEntityMarshallerTache: ToEntityMarshaller[Form] ): Route

}

trait DefaultApiMarshaller {

  implicit def toEntityMarshallerTache: ToEntityMarshaller[Tache]
  implicit def fromEntityUnmarshallerTache: FromEntityUnmarshaller[Tache]
  
  implicit def fromEntityUnmarshallerForm: FromEntityUnmarshaller[Form]
  implicit def toEntityMarshallerForm: ToEntityMarshaller[Form]
}

