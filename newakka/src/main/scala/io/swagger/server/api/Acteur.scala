package io.swagger.server.api

import akka.actor.typed.scaladsl.AskPattern._
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ ActorRef, ActorSystem }
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.util.Timeout
import spray.json.DefaultJsonProtocol._

import scala.concurrent.duration._
import scala.concurrent.{ ExecutionContext, Future }
import scala.io.StdIn

import io.swagger.server.model._
import akka.pattern.StatusReply

object server {
object MonActeur{

    trait Message

    
    case class GetTaches(replyTo: ActorRef[List[Tache]]) extends Message
    case class GetTache(id: Int,replyTo: ActorRef[Tache]) extends Message


    def apply: Behaviors.Receive[Message] = apply(List.empty)

    def apply(suggestions: List[Tache]): Behaviors.Receive[Message] = Behaviors.receive {
        case(ctx, suggestion @ Tache(id,module,date_debut,date_fin,description,realisee))=>
            ctx.log.info(s"Tache complete: $id,$module,$date_debut,$date_fin,$description,$realisee")
            apply(suggestions :+ suggestion)
        case(_, GetTaches(replyTo))=>
            replyTo ! suggestions
            Behaviors.same
        case(_, GetTache(id, replyTo) )=>
            replyTo ! suggestions.apply(id)
            Behaviors.same
    }
}
}